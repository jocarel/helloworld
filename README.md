**First**

Création d'un projet simple en java.

**Goal**

L'objectif est l'utilisation de l'outil d'intégration continue AutoDevops dans les projets à venir. 

**Build With**

- Eclispe is the IDE

**Author**

- **Jordan CAREL**

**License**

This project is licensed under the GNU GENERAL PUBLIC LICENSE
